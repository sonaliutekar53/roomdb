package com.app.roomdbkotlin

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.roomdbkotlin.R
import com.example.roomdatabase_kotlin.RecyclerAdapter
import com.example.roomdatabase_kotlin.ViewModel
import java.util.*

class PersonalActivity : AppCompatActivity() {

    lateinit var recyclerViewAdapter: RecyclerAdapter
    lateinit var viewModel: ViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_personal)

        val Recycle: RecyclerView = findViewById(R.id.Recycler)

        Recycle.apply {
            layoutManager = LinearLayoutManager(this@PersonalActivity)
            recyclerViewAdapter = RecyclerAdapter()
            adapter = recyclerViewAdapter

        }
        viewModel = ViewModelProviders.of(this).get(ViewModel::class.java)
        viewModel.getAllUsersObservers().observe(this, Observer {
            recyclerViewAdapter.setListData(ArrayList(it), viewModel)
            recyclerViewAdapter.notifyDataSetChanged()
        })
    }
}