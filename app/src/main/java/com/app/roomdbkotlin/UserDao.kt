package com.example.roomdatabase_kotlin.Data

import androidx.room.*

@Dao
interface UserDao {
    @Query("SELECT * FROM PersonalInfo ORDER BY id DESC")
    fun getAllUserInfo(): List<UserEntity>?


    @Insert
    fun insertUser(user: UserEntity?)

    @Delete
    fun Deleteuser(user: UserEntity?)

    @Update
    fun Updateuser(user: UserEntity?)
}