package com.app.roomdbkotlin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.lifecycle.ViewModelProviders
import com.example.roomdatabase_kotlin.Data.UserEntity
import com.example.roomdatabase_kotlin.ViewModel

class MainActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var viewModel: ViewModel
    lateinit var book_spinner: Spinner
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val Name: EditText = findViewById(R.id.Edt_name)
        val Mobile: EditText = findViewById(R.id.Edt_Mob)
        val edt_bookName: EditText = findViewById(R.id.edt_bookName)
        book_spinner = findViewById(R.id.book_spinner)
        val Save: Button = findViewById(R.id.SaveBtn)
        viewModel = ViewModelProviders.of(this).get(ViewModel::class.java)

        edt_bookName.setOnClickListener(this)

        val id: Int = intent.getIntExtra("id", 0)
        val ss: String = intent.getStringExtra("Name").toString()
        val ss1: String = intent.getStringExtra("Mob").toString()
        val ss2: String = intent.getStringExtra("bookname").toString()


        val Personlinfo = UserEntity(id, ss, ss1, ss2)

        if (!Personlinfo.name.equals("null")) {
            Name.setText(Personlinfo.name)
            Mobile.setText(Personlinfo.mob)
            edt_bookName.setText(Personlinfo.bookname)
        } else {
            Name.setText("")
            Mobile.setText("")
            edt_bookName.setText("")
        }

        val scondaryAdapter =
            ArrayAdapter<String>(this, R.layout.spinner_tv, viewModel!!.books)
        book_spinner.adapter = scondaryAdapter
        book_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {

                edt_bookName.setText(book_spinner.selectedItem.toString())

            }

        }


        Save.setOnClickListener {

            if (Personlinfo.id == 0) {
                val nameStr = Name.text.toString()
                val MobileStr = Mobile.text.toString()
                val bookNameStr = edt_bookName.text.toString()
                if (validate(nameStr, MobileStr, bookNameStr)) {
                    val Personlinfo = UserEntity(0, nameStr, MobileStr, bookNameStr)
                    viewModel.insertUserInfo(Personlinfo)
                    val intent = Intent(this, PersonalActivity::class.java)
                    startActivity(intent)
                }
            } else if (Personlinfo.id != 0) {
                val nameStr = Name.text.toString()
                val MobileStr = Mobile.text.toString()
                val bookNameStr = edt_bookName.text.toString()
                if (validate(nameStr, MobileStr, bookNameStr)) {
                    val Personlinfo = UserEntity(
                        intent.getIntExtra("id", 0),
                        nameStr,
                        MobileStr, bookNameStr
                    )
                    viewModel.updateUserInfo(Personlinfo)
                    val intent = Intent(this, PersonalActivity::class.java)
                    startActivity(intent)
                }
            }

        }
    }

    fun validate(name: String, mobile: String, bookname: String): Boolean {
        var check = true
        if (name!!.length == 0) {
            check = false
            Toast.makeText(this, "Please Enter Name", Toast.LENGTH_SHORT).show()
        }

        if (bookname!!.length == 0) {
            check = false
            Toast.makeText(this, "Please Select Book Name", Toast.LENGTH_SHORT).show()
        }

        if (mobile!!.length == 0) {
            check = false
            Toast.makeText(this, "Please Enter Mobile Number", Toast.LENGTH_SHORT)
                .show()
        } else if (mobile.length <= 9) {
            check = false
            Toast.makeText(this, "Please Enter Valid Mobile Number", Toast.LENGTH_SHORT)
                .show()
        }
        return check
    }

    override fun onClick(v: View?) {
        when (v!!.id) {

            R.id.edt_bookName -> {
                book_spinner.performClick()
            }
        }
    }
}