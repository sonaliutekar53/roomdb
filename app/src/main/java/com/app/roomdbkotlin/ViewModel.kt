package com.example.roomdatabase_kotlin

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.roomdatabase_kotlin.Data.RoomAppDb
import com.example.roomdatabase_kotlin.Data.UserEntity
import java.util.*

class ViewModel(app: Application): AndroidViewModel(app) {
    lateinit var allUsers : MutableLiveData<List<UserEntity>>
    var books = arrayOf("Java", "PHP", "Kotlin", "Javascript", "Python", "Swift")


    init{
        allUsers = MutableLiveData()
        getAllUsers()
    }

    fun getAllUsersObservers(): MutableLiveData<List<UserEntity>> {
        return allUsers
    }
    fun getAllUsers() {
        val userDao = RoomAppDb.getAppDatabase((getApplication()))?.userDao()
        val list = userDao?.getAllUserInfo()

        allUsers.postValue(list)
    }

    fun insertUserInfo(entity: UserEntity){
        val userDao = RoomAppDb.getAppDatabase(getApplication())?.userDao()
        userDao?.insertUser(entity)
        getAllUsers()
    }

    fun deleteUserInfo(entity: UserEntity){
        val delUserDao = RoomAppDb.getAppDatabase(getApplication())?.userDao()
        delUserDao?.Deleteuser(entity)
        getAllUsers()
    }

    fun updateUserInfo(entity: UserEntity){
        val delUserDao = RoomAppDb.getAppDatabase(getApplication())?.userDao()
        delUserDao?.Updateuser(entity)

    }

}