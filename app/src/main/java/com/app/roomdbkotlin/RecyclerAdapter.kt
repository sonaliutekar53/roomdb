package com.example.roomdatabase_kotlin

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.app.roomdbkotlin.MainActivity
import com.app.roomdbkotlin.R
import com.example.roomdatabase_kotlin.Data.UserEntity
import java.util.*


class RecyclerAdapter() : RecyclerView.Adapter<RecyclerAdapter.MyViewHolder>() {
    var items  = ArrayList<UserEntity>()
    lateinit var viewModel: ViewModel



    fun setListData(data: ArrayList<UserEntity>, view: ViewModel) {
        this.items = data
        this.viewModel=view
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.single_layout, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val DATA = items[position]
        holder.bind(items[position])
        holder.Del.setOnClickListener(){
            viewModel.deleteUserInfo(items[position])
        }
        holder.Linear.setOnClickListener(){
            val intent = Intent(holder.itemView.context, MainActivity::class.java)
            intent.putExtra("id",DATA.id)
            intent.putExtra("Name",DATA.name)
            intent.putExtra("Mob",DATA.mob)
            intent.putExtra("bookname",DATA.bookname)
            holder.itemView.context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class MyViewHolder(view: View): RecyclerView.ViewHolder(view) {

        val nameTxt :TextView = view.findViewById(R.id.nameTxt)
        val mobTxt :TextView = view.findViewById(R.id.MobTxt)
        val booknameTxt :TextView = view.findViewById(R.id.bookName)
        val Del : ImageView = view.findViewById(R.id.Delete)
        val Linear : LinearLayout = view.findViewById(R.id.LL)


        fun bind(data: UserEntity) {
            nameTxt.setText(data.name)
            mobTxt.setText(data.mob)
            booknameTxt.setText(data.bookname)
        }
    }
}